## Kontakt

<iframe id="map" width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?ll=51.93798,13.89063&t=h&output=embed&q=Puschkinstr.+12+15907+Lübben&hl=de&iwloc=false"></iframe>

Rechtsanwaltskanzlei Kristin Schenk<br>
Puschkinstr. 12<br>
15907 Lübben<br>

Telefon:  <a href="tel:+4935469347733">03546 / 934 77 33</a><br>
Fax:      <a href="tel:+4935469347722">03546 / 934 77 22</a><br>
Handy:    <a href="tel:+491743467992">0174 / 34 67 992</a><br>
Mail:     <a href="mailto:post@anwaltskanzlei-schenk.com">post@anwaltskanzlei-schenk.com</a>

## Lage

![Lage](img/lage1.jpg)
![Lage](img/lage2.jpg)
![Lage](img/lage3.jpg)
![Lage](img/lage4.jpg)
