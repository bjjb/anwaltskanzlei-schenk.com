![Photo](img/photo.jpg)

## Schön, dass Sie sich für uns interessieren.

Das dürfen Sie von uns erwarten:

Individuelle Betreuung durch gemeinsame Problemerkennung sowie konstruktive, kompetente und kostengünstige Problemlösungen.

Die Rechtsanwaltskanzlei Kristin Schenk steht dabei für:

1.	Kompetenz
2.	Erfahrung
3.	Erfolg
4.	Erreichbarkeit
5.	Freundlichkeit und
6.	Schnelligkeit.
